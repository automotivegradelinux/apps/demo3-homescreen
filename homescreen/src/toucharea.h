/*
 * Copyright (c) 2017-2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TOUCHAREA_H
#define TOUCHAREA_H

#include <QBitmap>
#include <QQuickWindow>

enum {
    NORMAL=0,
    FULLSCREEN
};

class TouchArea : public QObject
{
    Q_OBJECT
public:
    explicit TouchArea();
    ~TouchArea();

    Q_INVOKABLE void switchArea(int areaType);
    void setWindow(QQuickWindow* window);

public slots:
    void init();

private:
    QBitmap bitmapNormal, bitmapFullscreen;
    QQuickWindow* myWindow;
};

#endif // TOUCHAREA_H
